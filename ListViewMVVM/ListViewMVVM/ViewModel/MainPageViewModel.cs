﻿using ListViewMVVM.Model;
using System;
using System.Collections.ObjectModel;
using System.IO;
using Xamarin.Forms;

namespace ListViewMVVM.ViewModel
{
    public class MainPageViewModel
    {
        public ObservableCollection<Person> People { get; set; } = new ObservableCollection<Person>();
        public MainPageViewModel()
        {
            Random rand = new Random();
            for (int i = 1; i < 7; i++)
            {
                Person person = new Person()
                {
                    FirstName = "Anna ",
                    LastName = "Olsen" + i.ToString(),
                    Name = "Anna " + i.ToString(),
                    Address = i.ToString() + " Main Street",
                    Age = (decimal)(15 + 10 * rand.NextDouble()),
                    IconSource = @"https://www.codester.com/static/uploads/categories/29/icon.png"
                };
                People.Add(person);
            }
        }
    }
}
