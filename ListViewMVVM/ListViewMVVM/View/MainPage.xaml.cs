﻿using ListViewMVVM.Model;
using ListViewMVVM.ViewModel;
using Xamarin.Forms;

namespace ListViewMVVM
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            BindingContext = new MainPageViewModel();
        }

        private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return;
            }
            Person person = e.SelectedItem as Person;

            DisplayAlert("Selected", person.LastName, "OK");
        }
    }
}
