﻿namespace ListViewMVVM.Model
{
    public class Person
    {
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public decimal Age { get; set; }
        public string IconSource { get; set; }
    }
}
